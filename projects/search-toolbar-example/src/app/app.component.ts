import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  navLinks;
  title = 'app';

  constructor() {
    this.navLinks = [
      {
        label: 'Grid One',
        path: '/grid-one',
        index: 0
      }, {
        label: 'Grid Two',
        path: '/grid-two',
        index: 1
      }, {
        label: 'Grid Three',
        path: '/grid-three',
        index: 2
      }
    ];
  }

  ngOnInit(){
  }
}
