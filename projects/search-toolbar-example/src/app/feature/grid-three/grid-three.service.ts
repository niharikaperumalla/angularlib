import {Injectable} from '@angular/core';
import {CommonService} from "../common.service";

@Injectable({
  providedIn: 'root'
})
export class GridThreeService extends CommonService {

  constructor() {
    super('policies');
  }
}
