import { TestBed, inject } from '@angular/core/testing';

import { GridThreeService } from './grid-three.service';

describe('GridThreeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GridThreeService]
    });
  });

  it('should be created', inject([GridThreeService], (service: GridThreeService) => {
    expect(service).toBeTruthy();
  }));
});
