import { Component } from '@angular/core';
import { BaseGrid,TablePreferenceDelegate,TablePreferenceService } from "search-toolbar";
import { Observable } from "rxjs/index";
import { GridThreeService } from "./grid-three.service";

@Component({
  selector: 'app-grid-three',
  templateUrl: './grid-three.component.html',
  styleUrls: ['./grid-three.component.css']
})
export class GridThreeComponent extends BaseGrid {

  columnDefs = [
    { headerName: 'Id', field: 'id' },
    { headerName: 'Number', field: 'num' },
    { headerName: 'Amount', field: 'amount' },
    { headerName: 'User Id', field: 'userId' },
    { headerName: 'Client Id', field: 'clientId' },
    { headerName: 'Description', field: 'description' },
  ];
  componentRef: BaseGrid;
  searchFields = [
    { headerName: 'Number', field: 'num', type: 'text'},
    { headerName: 'Amount', field: 'amount', type: 'text'},
    { headerName: 'User Id', field: 'userId', type: 'text'},
    { headerName: 'Client Id', field: 'clientId', type: 'text'},
    { headerName: 'Description', field: 'description', type: 'string'},
  ]
  constructor( private viewService: GridThreeService) {
    super('grid-three');
    this.componentRef = this;
  }

  getData(event): Observable<any> {
    return this.viewService.get(event);
  }

  getSideNavData(): Observable<any>{
    return this.viewService.getSideNav();
  }

  add() {
    const selectedRow=this.gridOptions.api?this.gridOptions.api.getSelectedRows().length:0;
  }
}
