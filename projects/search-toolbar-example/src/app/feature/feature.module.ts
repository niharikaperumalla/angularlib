import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GridOneComponent} from './grid-one/grid-one.component';
import {GridTwoComponent} from './grid-two/grid-two.component';
import {GridThreeComponent} from './grid-three/grid-three.component';

import {FeatureRoutingModule} from "./feauture-routing/feauture-routing.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
// import {SideNavComponent} from './side-nav/side-nav.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import {DataService} from './in-memory/data.service';
import {SearchToolbarModule} from "search-toolbar";
import { CheckboxrenderComponent } from './checkboxrender/checkboxrender.component';

@NgModule({
  imports: [
    CommonModule,
    FeatureRoutingModule,
    HttpClientModule,
    SearchToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatOptionModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    InMemoryWebApiModule.forRoot(DataService, {passThruUnknownUrl: false})
  ],
  declarations: [GridOneComponent, GridTwoComponent, GridThreeComponent, CheckboxrenderComponent],
  // entryComponents: [DialogComponent],
  providers: [HttpClient]
})
export class FeatureModule { }
