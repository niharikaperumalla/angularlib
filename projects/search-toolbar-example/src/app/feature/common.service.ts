import {Observable} from "rxjs/index";
import {HttpClient} from "@angular/common/http";
import {AppInjector} from "../app-injector";

export abstract class CommonService {
  httpClient: HttpClient;
  SERVER_URL: string = "./";

  constructor(private url: string) {
    this.httpClient = AppInjector.get(HttpClient);
  }

  public get(criteria: any): Observable<any | null> {
   return this.httpClient.get(this.SERVER_URL + this.url + this.getQueryString(criteria));
    //return this.httpClient.get('./assets/policies.json');
  }

  public getSideNav(): Observable<any | null> {
    return this.httpClient.get(this.SERVER_URL + this.url + '');
    //return this.httpClient.get('./assets/policies.json');
  }

  private getQueryString(criteria: any): string {
    if(criteria) {
      let queryStr = '?';
      let criteriaMap = new Map(Object.entries(criteria))
      criteriaMap.forEach((value, key)=>{
      });
      criteriaMap.forEach((value, key) => {
        if(value) {
          queryStr += key + '=' + value + '&'
        }
      });
      return queryStr.substring(0, queryStr.length - 1);
    }
    return '';
  }
}
