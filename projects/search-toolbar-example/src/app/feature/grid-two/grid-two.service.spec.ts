import { TestBed, inject } from '@angular/core/testing';

import { GridTwoService } from './grid-two.service';

describe('GridTwoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GridTwoService]
    });
  });

  it('should be created', inject([GridTwoService], (service: GridTwoService) => {
    expect(service).toBeTruthy();
  }));
});
