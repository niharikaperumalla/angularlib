import { Component, ViewChild } from '@angular/core';
import { BaseGrid,TablePreferenceDelegate,TablePreferenceService } from "search-toolbar";
import { Observable } from "rxjs/index";
import { GridTwoService } from "./grid-two.service";

@Component({
  selector: 'app-grid-two',
  templateUrl: './grid-two.component.html',
  styleUrls: ['./grid-two.component.css']
})
export class GridTwoComponent extends BaseGrid {

  columnDefs = [
    { headerName: 'Id', field: 'id' },
    { headerName: 'Number', field: 'num' },
    { headerName: 'Amount', field: 'amount' },
    { headerName: 'User Id', field: 'userId' },
    { headerName: 'Client Id', field: 'clientId' },
    { headerName: 'Description', field: 'description' },
  ];
  searchFields = [
    { headerName: 'Number', field: 'num', type: 'text'},
    { headerName: 'Amount', field: 'amount', type: 'text'},
    { headerName: 'User Id', field: 'userId', type: 'text'},
    { headerName: 'Client Id', field: 'clientId', type: 'text'},
    { headerName: 'Description', field: 'description', type: 'string'},
  ]

  componentRef: BaseGrid;

  constructor(private viewService: GridTwoService) {
    super('grid-two');
    this.isSearchToolbarPresent=false;
    this.componentRef = this;
  }

  getData(event): Observable<any> {
    return this.viewService.get(event);
  }

  getSideNavData(): Observable<any>{
    return this.viewService.getSideNav();
  }
}
