import { Component } from '@angular/core';
import { Observable } from 'rxjs/index';
import { BaseGrid, TablePreferenceDelegate, TablePreferenceService} from "search-toolbar";
import { GridOneService } from './grid-one.service';
import { CheckboxrenderComponent } from '../checkboxrender/checkboxrender.component';


@Component({
  selector: 'app-grid-one',
  templateUrl: './grid-one.component.html',
  styleUrls: ['./grid-one.component.css']
})
export class GridOneComponent extends BaseGrid {
  columnDefs = [
    { headerName: 'Id', field: 'id' },
    { headerName: 'Number', field: 'num' },
    { headerName: 'Amount', field: 'amount' },
    { headerName: 'User Id', field: 'userId' },
    { headerName: 'Client Id', field: 'clientId' },
    { headerName: 'Description', field: 'description' },
    //cellRendererFramework:CheckboxrenderComponent
    { headerName: 'Status', field: 'active',cellRendererFramework:CheckboxrenderComponent}
  ];
  searchFields = [
    { headerName: 'Number', field: 'num', type: 'select'},
    { headerName: 'Amount', field: 'amount',type: 'radio'},
    { headerName: 'User Id', field: 'userId', type: 'text'},
    { headerName: 'Client Id', field: 'clientId', type: 'text',
    // options: [
    //   { value: 5, viewValue: 5 },
      // { value: 6, viewValue: 6 },
      // { value: 7, viewValue: 7 },
      // { value: 8, viewValue: 8 }
    //]
  },
    { headerName: 'Description', field: 'description', type: 'text'},
    { headerName: 'Status', field: 'active', type: 'checkbox'}
  ]
  componentRef: any;
  defaultSearchCriteria: any;
 
  constructor(private viewService: GridOneService) {
    super('grid-one');
    // this.setSearchCriteria({clientId: 7});
    this.componentRef = this;
    this.featureOptions.exportGridContentToExcel=false;
    //this.defaultSearchCriteria = { amount: "1000" };
  }

  getData(event): Observable<any> {
    return this.viewService.get(event);
  }
  getSideNavData(): Observable<any>{
    return this.viewService.getSideNav();
  }

  onRowDoubleClicked(event) {
    console.log('Double-clicked on this row: ');
    console.log(event);
  }

  onCellClicked(event) {
    console.log('Cell clicked');
    console.log(event);
  }

  onCellDoubleClicked(event) {
    console.log('Cell double clicked');
    console.log(event);
  }
}
