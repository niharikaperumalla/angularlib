import {Injectable, OnInit} from '@angular/core';
import {CommonService} from "../common.service";


@Injectable({
  providedIn: 'root'
})
export class GridOneService extends CommonService implements OnInit{

  constructor() {
    super('policies');
  }

  ngOnInit(){
  }
}
