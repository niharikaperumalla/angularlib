import { TestBed, inject } from '@angular/core/testing';

import { GridOneService } from './grid-one.service';

describe('GridOneService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GridOneService]
    });
  });

  it('should be created', inject([GridOneService], (service: GridOneService) => {
    expect(service).toBeTruthy();
  }));
});
