import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxrenderComponent } from './checkboxrender.component';

describe('CheckboxrenderComponent', () => {
  let component: CheckboxrenderComponent;
  let fixture: ComponentFixture<CheckboxrenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxrenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxrenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
