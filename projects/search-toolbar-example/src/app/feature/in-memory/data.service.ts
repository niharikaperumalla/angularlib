import { Injectable } from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService {

  constructor() { }

  createDb(){
    let  policies =  [
      {  id:  1,  num:  'PO1', amount: 4000, userId: 1, clientId: 6, description: 'Insurance policy number PO1', active: true },
      {  id:  2,  num:  'PO2', amount: 2000, userId: 1, clientId: 5, description: 'Insurance policy number PO2', active: false },
      {  id:  3,  num:  'PO3', amount: 3000, userId: 1, clientId: 8, description: 'Insurance policy number PO3', active: true },
      {  id:  4,  num:  'PO1', amount: 1000, userId: 1, clientId: 7, description: 'Insurance policy number PO4', active: false }
    ];



    let  searchviews =  [
      {  id:  1,  appName: 'CUSTOM-LIB', gridName: 'grid-one', name: 'Amount_2k', isDefault: true, userId: 96244, createdDate: '12/21/2018', searchCriteria: {
        amount: 2000
      } },
      {  id:  2,  appName: 'CUSTOM-LIB', gridName: 'grid-one', name: 'Amount_3k', isDefault: false, userId: 18054, createdDate: '12/21/2018', searchCriteria: {
        amount: 3000
      } }
    ];

    let  tableviews =  [
      {  id:  1,  appName: 'CUSTOM-LIB', gridName: 'grid-one', name: 'Preference-1', isDefault: false, userId: 96244, createdDate: '12/21/2018'},
      {  id:  2,  appName: 'CUSTOM-LIB', gridName: 'grid-one', name: 'Preference-2', isDefault: false, userId: 96244, createdDate: '12/21/2018'},
      {  id:  3,  appName: 'CUSTOM-LIB', gridName: 'grid-two', name: 'Preference-3', isDefault: false, userId: 96244, createdDate: '12/21/2018'},
      {  id:  4,  appName: 'CUSTOM-LIB', gridName: 'grid-two', name: 'Preference-4', isDefault: false, userId: 96244, createdDate: '12/21/2018'},
      {  id:  5,  appName: 'CUSTOM-LIB', gridName: 'grid-three', name: 'Preference-5', isDefault: false, userId: 96244, createdDate: '12/21/2018'},
      {  id:  6,  appName: 'CUSTOM-LIB', gridName: 'grid-one', name: 'AMOUNT SORT', isDefault: false, userId: 96244, createdDate: '12/21/2018',
      sortModel:[{colId: "amount", sort: "desc"}]},
      {  id:  7,  appName: 'CUSTOM-LIB', gridName: 'grid-one', name: 'CLIENTID SORT', isDefault: false, userId: 96244, createdDate: '12/21/2018',
      sortModel:[{colId: "clientId", sort: "desc"}]},
      {  id:  8,  appName: 'RATEAPPTP-LIBROVALAPP', gridName: 'grid-one', name: 'COLUMN PIN SORT', isDefault: true, userId: 96244, createdDate: '12/21/2018',
       columns: [
        {colId: "id", aggFunc: null,  hide: false, rowGroupIndex: null,    width: null, pinned: 'left'},
        {colId: "num", aggFunc: null,  hide: false, rowGroupIndex: null,    width: null, pinned: 'left'},
        {colId: "amount", aggFunc: null,  hide: false, rowGroupIndex: null,    width: null, pinned: null},
        {colId: "userId", aggFunc: null,  hide: false, rowGroupIndex: null,    width: null, pinned: null},
        {colId: "clientId", aggFunc: null,  hide: false, rowGroupIndex: null,    width: null, pinned: null},
        {colId: "description",     aggFunc: null,   hide: false,  rowGroupIndex: null, width: 600,  pinned: null}
      ],
      sortModel:[{colId: "id", sort: "desc"}]},
      {  id:  9,  appName: 'CUSTOM-LIB', gridName: 'grid-two', name: 'AMOUNT FILTER', isDefault: false, userId: 96244, createdDate: '12/21/2018',
      filterModel:{amount:{values:["2000"],filterType:"set"}}}

    ];

    return {policies, searchviews};

  }
}
