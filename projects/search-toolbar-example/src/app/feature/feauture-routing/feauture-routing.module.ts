import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {GridOneComponent} from "../grid-one/grid-one.component";
import {GridTwoComponent} from "../grid-two/grid-two.component";
import {GridThreeComponent} from "../grid-three/grid-three.component";


const featureRoutes: Routes = [
  {path: 'grid-one', component: GridOneComponent},
  {path: 'grid-two', component: GridTwoComponent},
  {path: 'grid-three', component: GridThreeComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(featureRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: []

})
export class FeatureRoutingModule { }
