import {BrowserModule} from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {MatTabsModule} from '@angular/material/tabs';
import {RouterModule, Routes} from "@angular/router";
import {FeatureModule} from "./feature/feature.module";
import {setAppInjector} from "./app-injector";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from "../environments/environment";
const appRoutes: Routes = [
  { path: '', redirectTo: 'grid-one', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MatTabsModule,
    FeatureModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true , useHash: true} // <-- debugging purposes only
    )
  ],
  providers: [
    {provide: 'environment', useValue: environment}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    setAppInjector(injector);
  }
 }
