export interface View {
  id: string;
  appName: string;
  gridName: string;
  name: string;
  createdBy: string;
  isDefault: boolean;
  createdAt: string;
}

export interface SearchView extends View {
  searchCriteria: object;
}

export interface GridView extends View {
  column: any[];
  columnGroup: any[];
  sortModel: any[];
  toolPanel: boolean;
}

export interface Preference extends  View {
  columns: object;
  columnGroups: object;
  sortModel: object;
  toolPanel: boolean;
}

export class OpenSearchCriteriaEvent {
  searchCriteria: object;
  isAdd: boolean;
}

export interface ImportExportParam {
  appName: string;
  enableImportExport: boolean;
  importUri?: string;
  exportUri?: string;
  templateUri?: string;
  params: any; // request body
}


export interface featuresFlags {
  exportGridContentToExcel: boolean;
  autoSizeColumns: boolean;
  resizeToFit: boolean;
  toolPanel: boolean;
  refreshTemplates: boolean;
  addRow: boolean;
}

