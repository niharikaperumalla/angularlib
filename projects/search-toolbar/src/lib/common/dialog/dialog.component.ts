import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import {ErrorStateMatcher } from '@angular/material/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validator, Validators} from '@angular/forms';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid);
    const invalidParent = !!(control && control.parent && control.parent.invalid);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'custom-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  title: String;
  label: string;

  myForm: FormGroup;

  matcher = new MyErrorStateMatcher();

  ngOnInit() {
  }

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private formBuilder: FormBuilder) {
    this.myForm = this.formBuilder.group({
      'name': ['', Validators.compose([Validators.required])],
      'viewNames': ['']
    }, { validator: this.validateViewName });

    this.myForm.setValue({
      name: this.data.name,
      viewNames: this.data.viewNames
    });

      if (data && data.isPreference) {
        this.title = 'Please Save Your Preference as ';
        this.label = 'Preference Name';
      } else {
        this.title = 'Please Save Your Search Template as ';
        this.label = 'Search Criteria Name';
      }

  }

  close(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.dialogRef.close(this.myForm.value.name);
  }

  validateViewName(group: FormGroup) {
    if (group.controls.viewNames.value.indexOf(group.controls.name.value) > -1) {
      return { isDuplicate: true };
    }
    if(group.controls.name.value.toUpperCase()==='DEFAULT'){
      return { isDefaultViewName: true };
    }
    return null;
  }
}
