import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {tap} from 'rxjs/internal/operators';
import {ImportExportParam} from '../../../model';
import {libEnvironment} from '../../../lib-environment';

@Injectable({
  providedIn: 'root'
})
export class ImportExportService {
  public static XLS = 'application/vnd.ms-excel';
  public static XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  private failedExcelBlob;
  public loadingSpinner = false;
  constructor(private http: HttpClient) {
  }

  public import(importExportParam: ImportExportParam, file: any): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    const options = {
      params: importExportParam.params ? importExportParam.params : {}
    };
    const uri = (importExportParam.importUri ? importExportParam.importUri : 'importexport/import/') + importExportParam.appName;
    return this.http.post(libEnvironment.baseUrl + uri, formData, options)
      .pipe(tap((response: any) => {
        this.failedExcelBlob = response.failedExcel;
      }));
  }

  public export(importExportParam: ImportExportParam, isTemplate: boolean): void {
    this.loadingSpinner = true;
    const data = importExportParam.params ? importExportParam.params : {};
    const options = {
      observe: 'response' as 'body',
      responseType: 'blob' as 'json'
    };
    const uriFromParam = isTemplate ? importExportParam.templateUri : importExportParam.exportUri;
    let uri = isTemplate ? 'importexport/template/' : 'importexport/export/';
    uri = (uriFromParam ? uriFromParam : uri) + importExportParam.appName;

    this.http.post(libEnvironment.baseUrl + uri, data, options).subscribe((response: any) => {
      const filename = this.getFileNameFromHttpResponse(response);
      this.downloadFile(response.body, filename);
      this.loadingSpinner = false;
    }, (error) => {
      console.log(error);
      this.loadingSpinner = false;
    });
  }

  public exportFailedData(): void {
    const byteCharacters = atob(this.failedExcelBlob);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    this.downloadFile(byteArray, 'failed-data.xls');
  }

  private getFileNameFromHttpResponse(httpResponse): string {
    const contentDispositionHeader = httpResponse.headers.get('Content-Disposition');
    const result = contentDispositionHeader.split(';')[1].trim().split('=')[1];
    return result.replace(/"/g, '');
  }

  private downloadFile(excelResponse, filename) {
    // It is necessary to create a new blob object with mime-type explicitly set
    // otherwise only Chrome works like it should
    const newBlob = new Blob([excelResponse], { type: ImportExportService.XLS });

    // IE doesn't allow using a blob object directly as link href
    // instead it is necessary to use msSaveOrOpenBlob
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(newBlob, filename);
      this.loadingSpinner = false;
      return;
    }

    // For other browsers:
    // Create a link pointing to the ObjectURL containing the blob.
    const data = window.URL.createObjectURL(newBlob);

    const link = document.createElement('a');
    link.href = data;
    link.download = filename;
    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

    setTimeout(function () {
      // For Firefox it is necessary to delay revoking the ObjectURL
      window.URL.revokeObjectURL(data);
      link.remove();
    }, 100);
    this.loadingSpinner = false;
  }
}
