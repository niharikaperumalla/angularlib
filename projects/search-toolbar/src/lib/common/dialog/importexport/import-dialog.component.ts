import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ImportExportParam} from '../../../model';
import {ImportExportService} from './import-export.service';

@Component({
  selector: 'custom-import-dialog',
  templateUrl: './import-dialog.component.html',
  styleUrls: ['./import-dialog.component.scss']
})
export class ImportDialogComponent implements OnInit {

  file: File;
  fileName;
  failedCount;
  importedCount;
  updatedCount;
  success = false;

  constructor(public dialogRef: MatDialogRef<ImportDialogComponent>, public importExportService: ImportExportService,
              private snackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: ImportExportParam) {
  }

  ngOnInit() {
  }

  onFileChange(event) {
    const f = event.target.files;
    if (f && f.length) {
      this.fileName = f[0].name;
      this.file = f[0];
    }
  }

  close() {
    this.dialogRef.close(true);
  }


  importData() {
      if (this.fileName.split('.').pop() !== 'xls' && this.fileName.split('.').pop() !== 'xlsx') {
        this.snackBar.open('Invalid file format!', 'Close', {duration: -1});
        return;
      }
    this.importExportService.loadingSpinner = true;
    this.importExportService.import(this.data, this.file).subscribe((response) => {
        this.success = true;
        this.importExportService.loadingSpinner = false;
        this.snackBar.open('Imported successfully', 'Close', {duration: 5000});
        this.failedCount = response.failedCount;
        this.importedCount = response.importedCount;
        this.updatedCount = response.updatedCount;
      },
      error => {
        this.importExportService.loadingSpinner = false;
        this.snackBar.open('Server error while importing data' + error, 'Close', {
          duration: -1
        });
      });
  }

  downloadTemplate() {
    this.importExportService.export(this.data, true);
  }

  downloadFailed(): void {
    this.importExportService.exportFailedData();
  }

  reset() {
    this.file = null;
    this.fileName = null;
    this.success = false;
    this.failedCount = null;
    this.importedCount = null;
    this.updatedCount = null;
  }
}
