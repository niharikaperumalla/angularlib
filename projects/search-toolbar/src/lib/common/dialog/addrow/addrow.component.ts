import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'lib-addrow',
  templateUrl: './addrow.component.html',
  styleUrls: ['./addrow.component.css']
})
export class AddrowComponent implements OnInit {
  searchFields;

  constructor(public dialogRef: MatDialogRef<AddrowComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private fb: FormBuilder) { 
          if(data){
          this.searchFields=data.formData;
          }
    }
  finalgroup={} 
  addForm: FormGroup;
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.check();
    this.initForm();
  }

  check(){
    // this.columnDefs.forEach(columns=>{
    //   this.finalgroup[columns.field]=new FormControl('');  
    // })
    for(let i=0;i<this.searchFields.length;i++){
      this.finalgroup[this.searchFields[i].field]=new FormControl('');
    }
  }

  initForm() {
      this.addForm = this.fb.group(
      this.finalgroup
      // 'id':[''],
      // 'num':[''],
      // 'amount': [''],
      // 'clientId': [''],
      // 'userId': [''],
      // 'description': ['']
  );
  }



}
