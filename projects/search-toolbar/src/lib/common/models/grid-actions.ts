
/** An interface that defines the actions that can be performed on the Grid inside the grid container. */
export enum GridActions {
    exportToExcel = 'exportToExcel',
    autoSizeAllColumns = 'autoSizeAllColumns',
    sizeColumnsToFit = 'sizeColumnsToFit',
    toggleToolPanel = 'toggleToolPanel',
    refreshGrid = 'refreshGrid'
  }
