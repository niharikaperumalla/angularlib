import {OnInit, Directive} from '@angular/core';
import {Observable, Subscription} from 'rxjs/index';
import {GridOptions} from 'ag-grid-community';
// import {LicenseManager} from 'ag-grid-enterprise';
import {GridActions} from '../common/models/grid-actions';
import {GridState} from '../common/grid-state';
import {TablePreferenceDelegate} from '../table-preference/table-preference.delegate';
import {LibInjector} from '../lib-injector';
import {Preference, featuresFlags} from '../model';
import { SidenavService } from '../sidenav/sidenav.service';


@Directive()
export abstract class BaseGrid implements OnInit {
  isDrawerOpened = false;
  showSpinner = false;
  isSearchToolbarPresent = true;
  searchCriteria: any = {};
  rowData = [];
  columnDefs: Array<any> = [];
  gridOptions: GridOptions;
  featureOptions: featuresFlags = {exportGridContentToExcel:true,
    refreshTemplates:true,
    resizeToFit:true,
    toolPanel:true,
    autoSizeColumns:true,
    addRow:true};

  public tablePreferenceDelegate: TablePreferenceDelegate;
  public sidenavService: SidenavService;
  selectedpreferenceSubscription: Subscription;


  constructor(public gridName: string) {
    // LicenseManager.setLicenseKey('Transplace_MultiApp_3Devs13_March_2019__MTU1MjQzNTIwMDAwMA==7d99255c3ed5462a8740611d30f399db');

     this.tablePreferenceDelegate = LibInjector.get(TablePreferenceDelegate);
     this.sidenavService= LibInjector.get(SidenavService);

  }

  abstract getData(event: any): Observable<any>;

  abstract getSideNavData(): Observable<any>;

  setSearchCriteria(searchCriteria: any): void {
    this.searchCriteria = searchCriteria;
  }

  ngOnInit() {
    this.initGridOptions();
    // trigger search if search toolbar is absent
    if (!this.isSearchToolbarPresent) {
      this.refreshGrid(this.searchCriteria);
    }
  }

  initGridOptions() {
    this.gridOptions = GridState.GetDefaults(this.tablePreferenceDelegate, this.gridName);
    this.gridOptions.context = {
      componentParent: this,
      loading: true
    };
    if (this.tablePreferenceDelegate) {
      this.selectedpreferenceSubscription = this.tablePreferenceDelegate.selectedPreferenceSubject.subscribe(data => {
        this.tablePreferenceDelegate.setViewPreference(this.gridOptions, data);
      });
    }
    this.gridOptions.context = this;
  }

  // apply selected table preference manually using this method
  applyTablePreference() {
    const p: Preference = this.tablePreferenceDelegate.getSelectedViewPreference(this.gridName);
    this.tablePreferenceDelegate.getPreferencesViewSubject().subscribe((preferences: Array<Preference>) => {
      preferences.forEach((preference) => {
        if (p && p.id && p.id === preference.id) {
          this.tablePreferenceDelegate.setViewPreference(this.gridOptions, preference);
        }
      });
    });
  }

  /**search tool bar methods */
  // onSearchCriteriaClick(event) {
  //   this.searchCriteria = event.searchCriteria;
  //   this.isDrawerOpened = event.isAdd ? true : !this.isDrawerOpened;
  // }

  onSearchCriteriaReset(event) {
    this.searchCriteria = event;
  }

  /**end search tool bar methods */

  /**
   * RefreshGrid Action
   * @param event
   */
  refreshGrid(event) {
    this.searchCriteria = event;
    this.showSpinner = true;
    this.getSideNavData().subscribe(data => {
      this.sidenavService.setSidenavData(data);
    });
    this.getData(event).subscribe(data => {
      this.showSpinner = false;
      this.rowData = data;
    },
      (error) => {
        console.log(error);
        this.showSpinner = false;
      });
  }

  /**
  * Grid Actions
  * @param $event
  */
  onGridActionButtonClicked($event: GridActions) {
    switch ($event) {
      case GridActions.refreshGrid:
        this.refreshGrid(this.searchCriteria);
    }
  }

  // ngOnDestroy() {
  //   this.selectedpreferenceSubscription.unsubscribe();
  // }
}
