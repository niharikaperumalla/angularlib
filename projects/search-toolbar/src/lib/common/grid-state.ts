import {ColumnEverythingChangedEvent, GridOptions, GridReadyEvent, SortChangedEvent} from 'ag-grid-community';

export class GridState {
  static GetDefaults(tablePreferenceDelegate, gridName): GridOptions {

    let gridOptions: GridOptions = null;

    const saveColumnState = () => {
      if (gridOptions === null || gridOptions === undefined) {
        return;
      }
      const cs = gridOptions.columnApi.getColumnState();
      tablePreferenceDelegate.setGridState(gridName + '-GRID-COLUMNS', cs);
    };

    const saveColumnGroupState = () => {
      tablePreferenceDelegate.setGridState(gridName + '-GRID-COLUMNS-GROUPS', gridOptions.columnApi.getColumnGroupState());
    };

    gridOptions = <GridOptions>{

      enableSorting : true,
      enableFilter : true,
      enableColResize : true,

      onSortChanged: (event: SortChangedEvent) => {
        tablePreferenceDelegate.setGridState(gridName + '-GRID-SORT', event.api.getSortModel());
      },
      onColumnResized: () => saveColumnState(),
      onColumnGroupOpened: () => saveColumnGroupState(),
      onColumnMoved: () => saveColumnState(),
      onColumnPinned: () => saveColumnState(),
      onColumnVisible: () => saveColumnState(),
      onColumnPivotChanged: () => saveColumnState(),
      onColumnRowGroupChanged: () => {
        saveColumnState();
        saveColumnGroupState();
      },
      onGridReady: (event: GridReadyEvent) => {
        gridOptions.onColumnEverythingChanged = (everythingEvent: ColumnEverythingChangedEvent) => {
          if (gridOptions.context.loading) {
            gridOptions.context.loading = false;
            return;
          }
        };
      }
    };
    gridOptions.rowData = <any>[];
    return gridOptions;
  }

}
