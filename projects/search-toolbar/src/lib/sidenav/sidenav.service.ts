import {Injectable} from '@angular/core';
import { Subject, Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  private sideNavSubject: Subject<any> = new Subject();

  public setSidenavData(data){
    this.sideNavSubject.next(data);
  }

  getSidenavDataObservable(): Observable<any> {
    return this.sideNavSubject.asObservable();
  }
  
  
}
