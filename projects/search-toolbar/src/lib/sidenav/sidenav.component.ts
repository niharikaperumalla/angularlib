// import { Component, OnInit } from '@angular/core';
import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, FormControl} from '@angular/forms';
// import {SearchToolbarService} from 'search-toolbar';
import {MatDrawer} from '@angular/material/sidenav';
import { SearchToolbarService } from '../search-toolbar/search-toolbar.service';
import { SidenavService } from './sidenav.service';

@Component({
  selector: 'lib-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  
  @Input() searchCriteria: any;
  @Input() drawer: MatDrawer;
  @Input() isOpened: boolean;
  @Input() searchFields: any;
  sideNaveForm: FormGroup;
   finalgroup={} 
   sidenavData;
  constructor(private fb: FormBuilder, private searchToolbarService: SearchToolbarService,private sidenavService:SidenavService) {
  }

  ngOnInit() {
    this.sidenavService.getSidenavDataObservable().subscribe(data => {
      this.sidenavData=data;
      this.addOptionsField(this.sidenavData);
     });
    this.formControlsbuilder();
    this.initForm();
  }
  
  addOptionsField(data){
    for(let i=0;i<this.searchFields.length;i++){
      if(this.searchFields[i].type=="select"||this.searchFields[i].type=="radio")
      {
         const key=this.searchFields[i].field;
         const uniqueArray=[...new Set(this.sidenavData.map(y => y[key]))];
         var options = [];
         for (let i = 0; i < uniqueArray.length; i++) {
         let properties = {
          value:uniqueArray[i],
          viewValue:uniqueArray[i]
         };
         options.push(properties);
         }
         this.searchFields[i].options=options;
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.searchCriteria) {
      this.sideNaveForm.reset({});
      this.sideNaveForm.patchValue(this.searchCriteria);
    }

    this.drawer.opened = this.isOpened;
  }
 
  formControlsbuilder(){
     for(let i=0;i<this.searchFields.length;i++){
      if(this.searchFields[i].type=="checkbox"){
        this.finalgroup[this.searchFields[i].field]=new FormControl('');
      }
      else{
      this.finalgroup[this.searchFields[i].field]=new FormControl('');
      }
    }
  }

  initForm() {
      this.sideNaveForm = this.fb.group(
      this.finalgroup
      // 'id':[''],
      // 'num':[''],
      // 'amount': [''],
      // 'clientId': [''],
      // 'userId': [''],
      // 'description': ['']
  );
  }

onChange(event: any,field:string)
{
console.log(event);
console.log(field);
console.log(event.source.value); // will contain "hello"
console.log(event.source.checked); // will contain the checked state of the checkbox
}
  search(form) {
    console.log("In Sidenav Component");
    console.log(form);
    console.log(form.value);
    this.searchToolbarService.refresh(form);
  }

  save(form) {
    this.searchToolbarService.save(form);
  }

  reset() {
    this.searchToolbarService.reset();
  }

}
