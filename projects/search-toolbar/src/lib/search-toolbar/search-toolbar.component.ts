import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { SearchToolbarService } from './search-toolbar.service';
import { Subscription } from 'rxjs';
import { ViewDataService } from './view-data.service';
import { SearchToolbarDelegate } from './search-toolbar.delegate';

import { OpenSearchCriteriaEvent } from '../model';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'custom-search-toolbar',
  templateUrl: './search-toolbar.component.html',
  styleUrls: ['./search-toolbar.component.css']
})
export class SearchToolbarComponent implements OnInit, OnDestroy {
  markUnmarkIconName = 'star';

  @Input() appName: string;
  @Input() gridName: string;
  @Input() userId: string;
  @Input() defaultSearchCriteria: any;

  @Output() onSearchCriteriaClick: EventEmitter<any> = new EventEmitter();
  @Output() onSearchCriteriaChange: EventEmitter<any> = new EventEmitter();

  searchToolbarDelegate: SearchToolbarDelegate;

  refreshObservableSubscription: Subscription;
  resetObservableSubscription: Subscription;
  addCriteriaObservableSubscription: Subscription;

  constructor(private viewDataService: ViewDataService, public dialog: MatDialog, public matSnackbarService: MatSnackBar) { }

  ngOnInit() {


    this.searchToolbarDelegate = new SearchToolbarDelegate(this.appName, this.gridName, this.userId, this.viewDataService, this.dialog, this.matSnackbarService, this.defaultSearchCriteria);

    this.refreshObservableSubscription = this.searchToolbarDelegate.getRefreshObservable().subscribe(sc => {
      if (sc) {
        this.search(sc);
      }
    });

    this.resetObservableSubscription = this.searchToolbarDelegate.getResetObservable().subscribe(sc => {
      this.search(sc);
    });

    this.addCriteriaObservableSubscription = this.searchToolbarDelegate.getAddCriteriaObservable().subscribe(sc => {
      this.onOpenSearchCriteriaBtnClick({ searchCriteria: sc, isAdd: true });
    });

  }

  search(searchCriteria: any) {
    this.onSearchCriteriaChange.emit(searchCriteria);
  }

  onSearchCriteriaBtnClick(event) {
    this.onOpenSearchCriteriaBtnClick({ searchCriteria: this.searchToolbarDelegate.selectedSearchView.searchCriteria, isAdd: false });
  }

  private onOpenSearchCriteriaBtnClick(event: OpenSearchCriteriaEvent) {
    this.onSearchCriteriaClick.emit(event);
  }

  ngOnDestroy() {
    this.searchToolbarDelegate.onDestroy();
    this.addCriteriaObservableSubscription.unsubscribe();
    this.refreshObservableSubscription.unsubscribe();
    this.resetObservableSubscription.unsubscribe();
  }
}
