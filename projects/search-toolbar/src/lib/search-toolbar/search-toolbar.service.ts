import {Injectable} from '@angular/core';
import {ViewDataService} from './view-data.service';

@Injectable({
  providedIn: 'root'
})
export class SearchToolbarService {

  constructor(private viewDataService: ViewDataService) { }

  public refresh(searchCriteria: any) {
    this.viewDataService.refresh(searchCriteria);
  }

  public save(searchCriteria: any) {
    this.viewDataService.save(searchCriteria);
  }

  public reset() {
    this.viewDataService.reset();
  }

  public resetToDefaultTemplate() {
    this.viewDataService.resetToDefaultTemplate();
  }

}
