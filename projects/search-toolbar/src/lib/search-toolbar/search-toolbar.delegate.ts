import {BehaviorSubject, Observable, Subject, Subscription} from 'rxjs';
import {ViewDataService} from './view-data.service';
import {SearchView} from '../model';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DialogComponent} from '../common/dialog/dialog.component';

export class SearchToolbarDelegate {

  DEFAULT_NAME = 'Default';
  DEFAULT_ID = 'default';

  public searchViews: SearchView[] = [];
  public selectedSearchView: SearchView;

  private refreshSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  private resetSubject: Subject<any> = new Subject();
  private addCriteriaSubject: Subject<any> = new Subject();

  private searchCriteriaObservableSub: Subscription;
  private searchViewObservableSub: Subscription;
  private resetObservableSub: Subscription;
  private resetToDefaultTemplateObservableSub: Subscription;
  private saveViewObservableSub: Subscription;

  constructor(private appName: string, private gridName: string, private userId: string, private viewDataService: ViewDataService, public dialog: MatDialog, public matSnackbar: MatSnackBar, private defaultSearchCriteria: any) {
    this.searchViewObservableSub = this.viewDataService.getSearchViewObservable().subscribe(views => {
      if (views) {
        views = this.addDefaultView(views);
        this.searchViews = views.filter(view => view.gridName == this.gridName);
        const defaultViews = this.searchViews.filter(view => view.isDefault);
        const defaultSearchView = defaultViews && defaultViews.length > 0 ? defaultViews[0] : null;
        if (!this.viewDataService.getSelectedSearchView(this.gridName)) {
          if (defaultSearchView) {
            this.viewDataService.setSelectedSearchView(this.gridName, defaultSearchView);
          } else {
            const searchViews: SearchView[] = views.filter(v => v.id === this.DEFAULT_ID);
            this.viewDataService.setSelectedSearchView(this.gridName, searchViews[0]);
          }
        }
        this.selectedSearchView = this.viewDataService.getSelectedSearchView(this.gridName);
        this.refreshSubject.next(this.selectedSearchView.searchCriteria);
      }
    });
      this.searchCriteriaObservableSub = this.viewDataService.getCriteriaObservable().subscribe(searchCriteria => {
      this.selectedSearchView.searchCriteria = searchCriteria;
      this.viewDataService.setSelectedSearchView(this.gridName, this.selectedSearchView);
      this.refreshSubject.next(searchCriteria);
    });

    this.resetObservableSub = this.viewDataService.getResetObservable().subscribe(() => {
      if (this.selectedSearchView.id) {
        if (this.selectedSearchView.id === this.DEFAULT_ID) {
          this.selectedSearchView.searchCriteria = this.defaultSearchCriteria ? Object.assign({}, this.defaultSearchCriteria) : {};
        } else {
          const searchView = this.viewDataService.getView(this.selectedSearchView.id);
          if (searchView) {
            this.selectedSearchView.searchCriteria = searchView.searchCriteria;
          }
        }
      } else {
        this.selectedSearchView.searchCriteria = {};
      }
      this.viewDataService.setSelectedSearchView(this.gridName, this.selectedSearchView);
      this.resetSubject.next(this.selectedSearchView.searchCriteria);
    });

    this.resetToDefaultTemplateObservableSub = this.viewDataService.getResetToDefaultTemplateObservable().subscribe(() => {
      const searchView = this.searchViews.filter(view => view.id === 'default');
      this.selectedSearchView = searchView[0];
      this.viewDataService.setSelectedSearchView(this.gridName, this.selectedSearchView);
      this.resetSubject.next(this.selectedSearchView.searchCriteria);
    });

    this.saveViewObservableSub = this.viewDataService.getSaveViewObservable().subscribe(sc => {
      const searchView = Object.assign({}, this.selectedSearchView);
      searchView.searchCriteria=sc;
      if (!searchView.id) {
        const dialogRef = this.dialog.open(DialogComponent, {
          width: '400px',
          // name: searchView.name,
           data: {  name: searchView.name, viewNames: this.searchViews.map(v => v.name) }
        });

        dialogRef.afterClosed().subscribe(name => {
          if (name) {
            searchView.name = name;
            this.viewDataService.saveView(searchView);
          }
        });
      } else {
        if (searchView.id === this.DEFAULT_ID) {
          this.matSnackbar.open('Saving Default Search Criteria is not allowed', 'Close', { duration: 5000 });
        } else {
          this.viewDataService.update(searchView, searchView.id);
        }
      }
    });

    this.viewDataService.getViews(this.appName, this.userId);
  }

  public onDelete(event) {
    this.viewDataService.deleteView(this.selectedSearchView.id);
  }

  public markUnmarkDefault(event) {
    this.selectedSearchView.isDefault = !this.selectedSearchView.isDefault;
    this.viewDataService.update(this.selectedSearchView, this.selectedSearchView.id);
    this.viewDataService.setSelectedSearchView(this.gridName, this.selectedSearchView);
  }

  public onClone(event) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px',
      data: { name: this.selectedSearchView.name, viewNames: this.searchViews.map(v => v.name) }
    });

    dialogRef.afterClosed().subscribe(name => {
      if (name) {
        const searchView = Object.assign({}, this.selectedSearchView);
        searchView.name = name;
        searchView.createdBy = '' + this.userId;
        searchView.isDefault = false;
        delete searchView.id;
        this.viewDataService.saveView(searchView);
      }
    });
  }

  onAdd(event) {

    const searchView = this.viewDataService.getEmptySearchView(this.appName, this.gridName, this.userId);
    this.selectedSearchView = searchView;

    /**If default Search Criteria available setting the default value */
    if (this.defaultSearchCriteria) {
      const searchCriteria = Object.assign({}, this.defaultSearchCriteria);
      delete searchCriteria.id;
      this.addCriteriaSubject.next(searchCriteria);
    } else {
      this.addCriteriaSubject.next(searchView.searchCriteria);
    }
  }

  public onSaveOrUpdate(event) {
    this.viewDataService.update(this.selectedSearchView, this.selectedSearchView.id);
  }

  public onSelectionChange(event) {
    this.viewDataService.setSelectedSearchView(this.gridName, this.selectedSearchView);
    this.refreshSubject.next(this.selectedSearchView.searchCriteria);
  }

  addDefaultView(views: SearchView[]): SearchView[] {
    if (!views) {
      views = [];
    }
    const defaultView = this.viewDataService.getEmptySearchView(this.appName, this.gridName, this.userId);
    defaultView.name = this.DEFAULT_NAME;
    defaultView.id = this.DEFAULT_ID;
    defaultView.searchCriteria = this.defaultSearchCriteria ? this.defaultSearchCriteria : {};
    views.push(defaultView);
    return views;
  }

  public getRefreshObservable(): Observable<any> {
    return this.refreshSubject.asObservable();
  }
  public getResetObservable(): Observable<any> {
    return this.resetSubject.asObservable();
  }
  public getAddCriteriaObservable(): Observable<any> {
    return this.addCriteriaSubject.asObservable();
  }

  isDisabled() {
    if (this.selectedSearchView) {
      return !this.selectedSearchView.id || this.selectedSearchView.id === 'default';
    }
    return true;
  }

  compareFn(ob1, ob2) {
    return ob1 && ob2 ? ob1.id == ob2.id : ob1 == ob2;
  }

  public onDestroy() {
    this.searchCriteriaObservableSub.unsubscribe();
    this.searchViewObservableSub.unsubscribe();
    this.resetObservableSub.unsubscribe();
    this.saveViewObservableSub.unsubscribe();
  }

}
