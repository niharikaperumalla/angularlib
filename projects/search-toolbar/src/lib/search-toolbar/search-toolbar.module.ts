import {NgModule} from '@angular/core';
import {SearchToolbarComponent} from './search-toolbar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {DialogComponent} from '../common/dialog/dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    MatIconModule,
    MatDividerModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    FormsModule,
    MatToolbarModule,
    FontAwesomeModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule
  ],
  declarations: [SearchToolbarComponent, DialogComponent],
  exports: [SearchToolbarComponent]
})
export class SearchToolbarModule { }
