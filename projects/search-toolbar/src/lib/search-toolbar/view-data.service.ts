import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {SearchView} from '../model';
import {CommonService} from '../common.service';

@Injectable({
  providedIn: 'root'
})
export class ViewDataService extends CommonService {
  public selectedSearchViewMap: Map<string, SearchView> = new Map();
  public sideNavData;
  private criteriaSubject: Subject<any> = new Subject();
  private searchViewsSubject: Subject<SearchView[]> = new Subject();
  private resetViewSubject: Subject<any> = new Subject();
  private resetToDefaultTemplateSubject: Subject<any> = new Subject();
  private saveViewSubject: Subject<any> = new Subject();

  private searchViews: SearchView[] = [];

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  getViews(appName: string, userId: string): void {
    this.get(appName, userId).subscribe(views => {
      this.searchViews = views ? views : [];
      this.searchViewsSubject.next(JSON.parse(JSON.stringify(views)));
    });
  }

  saveView(view: SearchView): any {
    this.post(view).subscribe(v => {
      this.setSelectedSearchView(v.gridName, v);
      this.searchViews.push(v);
      this.searchViewsSubject.next(JSON.parse(JSON.stringify(this.searchViews)));
    });
  }

  update(view: SearchView, id: string) {
    this.patch(view, id).subscribe(searchView => {
      this.setSelectedSearchView(view.gridName, view);
      this.getViews(view.appName, view.createdBy);
    });
  }

  deleteView(id: string) {
    this.delete(id).subscribe(view => {
      const deletedView: SearchView[] = this.searchViews.filter(v => v.id === id);
      if (deletedView && deletedView.length > 0) {
        const index = this.searchViews.indexOf(deletedView[0]);
        if (index > -1) {
          this.searchViews.splice(index, 1);
          this.selectedSearchViewMap.delete(deletedView[0].gridName);
          this.searchViewsSubject.next(JSON.parse(JSON.stringify(this.searchViews)));
        }
      }
    });
  }

  getView(id: string): SearchView {
    const views = this.searchViews.filter(view => view.id === id);
    if (views && views.length > 0) {
      return Object.assign({}, views[0]);
    }
    return null;
  }

  getSearchViewObservable(): Observable<any> {
    return this.searchViewsSubject.asObservable();
  }

  getCriteriaObservable(): Observable<any> {
    return this.criteriaSubject.asObservable();
  }

  getResetObservable(): Observable<any> {
    return this.resetViewSubject.asObservable();
  }

  getResetToDefaultTemplateObservable(): Observable<any> {
    return this.resetToDefaultTemplateSubject.asObservable();
  }

  getSaveViewObservable(): Observable<any> {
    return this.saveViewSubject.asObservable();
  }

  public setSelectedSearchView(viewName: string, searchView: SearchView) {
    this.selectedSearchViewMap.set(viewName, searchView);
  }

  public getSelectedSearchView(viewName: string) {
    return this.selectedSearchViewMap.get(viewName);
  }

  public refresh(searchCriteria: any) {
    this.criteriaSubject.next(searchCriteria);
  }

  public save(searchCriteria: any) {
    this.saveViewSubject.next(searchCriteria);
  }

  public reset() {
    this.resetViewSubject.next();
  }

  public resetToDefaultTemplate() {
    this.resetToDefaultTemplateSubject.next();
  }

  public getEmptySearchView(appName: string, gridName: string, userId: string): SearchView {
    return {
      id: null,
      gridName: gridName,
      appName: appName,
      name: '',
      createdBy: '' + userId,
      createdAt: '',
      isDefault: false,
      searchCriteria: {},
    } as SearchView;
  }
}
