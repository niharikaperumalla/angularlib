import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SearchView} from './model';
import {HttpClient} from '@angular/common/http';
import {libEnvironment} from './lib-environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  url = 'searchviews';
  constructor(private httpClient: HttpClient) {
  }

  public get(appName: string, userId: string): Observable<SearchView[] | null> {
    return this.httpClient.get<SearchView[]>(libEnvironment.baseUrl + this.url, {
      params: {
        appName: appName,
        userId: userId
      }
    });
  }

  public post(view: SearchView): Observable<SearchView | null> {
    return this.httpClient.post<SearchView>(libEnvironment.baseUrl + this.url, view, {
      params: {
        userId: view.createdBy
      }
    });
  }

  public put(view: SearchView, id: string): Observable<SearchView | null> {
    return this.httpClient.put<SearchView>(libEnvironment.baseUrl + this.url + '/' + id, view);
  }

  public patch(view: any, id: string): Observable<SearchView | null> {
    return this.httpClient.patch<SearchView>(libEnvironment.baseUrl + this.url + '/' + id, view);
  }

  public delete(id: string): Observable<SearchView | null> {
    return this.httpClient.delete<SearchView>(libEnvironment.baseUrl + this.url + '/' + id);
  }
}
