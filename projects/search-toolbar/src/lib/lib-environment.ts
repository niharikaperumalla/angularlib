// This is injected from the parent app where library is used. see NgConfigToolLibModule's constructor
// Make sure to set the provider correctly in your app for injection to work. Provider is required.
export let libEnvironment: any = {baseUrl: './'};

export function setLibEnvironment(env: any) {
  libEnvironment = env;
}
