import {Inject, Injector, NgModule} from '@angular/core';
import {GridContainerComponent} from './grid-container/grid-container.component';
import {SearchToolbarComponent} from './search-toolbar/search-toolbar.component';
import {DialogComponent} from './common/dialog/dialog.component';
import {SpinnerComponent} from './common/components/spinner/spinner.component';
import { AgGridModule } from 'ag-grid-angular';
import { MatButtonModule } from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatToolbarModule} from '@angular/material/toolbar'; 
import {MatRadioModule} from '@angular/material/radio';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {setLibInjector} from './lib-injector';
import {setLibEnvironment} from './lib-environment';
import {ImportDialogComponent} from './common/dialog/importexport/import-dialog.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { AddrowComponent } from './common/dialog/addrow/addrow.component';
const agGridComponents = [GridContainerComponent];

@NgModule({
  imports: [
    ReactiveFormsModule,
    AgGridModule.withComponents(agGridComponents),
    CommonModule,
    MatIconModule,
    MatDividerModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatSidenavModule,
    MatRadioModule,
    MatCheckboxModule,
    FormsModule,
    MatToolbarModule,
    FontAwesomeModule,
    MatButtonModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatIconModule
  ],
  declarations: [GridContainerComponent, SearchToolbarComponent, DialogComponent, SpinnerComponent, ImportDialogComponent, SidenavComponent, AddrowComponent],
  exports: [GridContainerComponent, SearchToolbarComponent, DialogComponent, SpinnerComponent],
  entryComponents: [DialogComponent, ImportDialogComponent]
})
export class SearchToolbarModule {
  constructor(injector: Injector, @Inject('environment')
  appEnvironment) {
  setLibInjector(injector);
  if (appEnvironment && appEnvironment.baseUrl) {
    setLibEnvironment(appEnvironment);
  }
}
 }
