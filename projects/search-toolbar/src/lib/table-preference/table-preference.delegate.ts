import {BehaviorSubject, Observable} from 'rxjs';
import {Preference} from '../model';
import {Constants} from '../constants';
import {DialogComponent} from '../common/dialog/dialog.component';
import {Injectable} from '@angular/core';
import {TablePreferenceService} from './table-preference.service';

@Injectable({
  providedIn: 'root'
})
export class TablePreferenceDelegate {

  public selectedViewPreference: Map<string, any> = new Map();
  public tablePreferences = new Array<Preference>();
  public viewPreferencesSubject: BehaviorSubject<Array<Preference>> = new BehaviorSubject([]);
  public selectedPreferenceSubject: BehaviorSubject<Preference> = new BehaviorSubject(null);

  constructor(private tablePreferenceService: TablePreferenceService) {
    //this.getPreferences();
    //TODO:
  }

  getPreferences(appName, gridName, userId) {
    this.tablePreferenceService.get(appName, gridName, userId).subscribe(preferences => {
      this.tablePreferences = preferences;
      this.viewPreferencesSubject.next(this.tablePreferences);
    });
  }

  getPreferencesViewSubject(): Observable<any> {
    return this.viewPreferencesSubject.asObservable();
  }

  getSelectedPreference(): Observable<any> {
    return this.selectedPreferenceSubject.asObservable();
  }

  /***
   * preserve the grid state using current state object
   */
  setViewPreference(gridOptions, currentState) {
    if (gridOptions.columnApi) {
      setTimeout(function () {
        if (currentState) {
          if (currentState.name === Constants.DEFAULT_TEMPLATE) {
            gridOptions.columnApi.resetColumnState();
            gridOptions.api.setFilterModel(null);
            gridOptions.api.setSortModel(null);
            gridOptions.columnApi.resetColumnGroupState();
          } else {
            gridOptions.api.setSideBarVisible(currentState.toolPanel);
            if (currentState.sortModel && currentState.sortModel.length > 0) {
              gridOptions.api.setSortModel(currentState.sortModel);
            } else {
              gridOptions.api.setSortModel(null);
            }
            if (currentState.columns) {
              gridOptions.columnApi.setColumnState(currentState.columns);
            } else {
              gridOptions.columnApi.resetColumnState();
            }
            if (currentState.columnGroupState) {
              gridOptions.columnApi.setColumnGroupState(currentState.columnGroupState);
            } else {
              gridOptions.columnApi.resetColumnGroupState();
            }
            if (currentState.filterModel && currentState.filterModel.length > 0) {
              gridOptions.api.setFilterModel(currentState.filterModel);
            } else {
              gridOptions.api.setFilterModel(null);
            }
          }
        }
      }, 500);
    }
  }

  private getState(gridOptions) {
    if (gridOptions) {
      return {
        'columns': gridOptions.columnApi.getColumnState(),
        'columnGroups': gridOptions.columnApi.getColumnGroupState(),
        'sortModel': gridOptions.api.getSortModel(),
        'toolPanel': gridOptions.api.isToolPanelShowing(),
        'filterModel' : gridOptions.api.getFilterModel()
      };
    }
  }


  public getGridState(key) {
    return this.tablePreferenceService.getGridState().get(key);
  }

  public setGridState(key, value) {
    return this.tablePreferenceService.getGridState().set(key, value);
  }


  public setSelectedViewPreference(gridName, id, name, userId, appName, isDefault) {
    const preference: Preference = this.tablePreferenceService.getEmptyPreference(id, name, appName, gridName, userId);
    preference.id = id;
    preference.isDefault = isDefault;
    this.selectedViewPreference.set(gridName, preference);
    return preference;
  }

  public getSelectedViewPreference(gridName) {
    return this.selectedViewPreference.get(gridName);
  }

  saveViewPreference(userId, appData, gridOptions): Observable<any | null> {
    if (appData) {
      const preference = this.buildPreference(appData, gridOptions);
      return this.tablePreferenceService.save(userId, preference);
    }
  }

  updateViewPreference(appData, gridOptions): Observable<any | null> {
    if (appData) {
      const preference = this.buildPreference(appData, gridOptions);
      return this.tablePreferenceService.update(preference.id, preference);
    }

  }

  deleteViewPreference(preference): Observable<any | null> {
    return this.tablePreferenceService.delete(preference.id);
  }

  updateViewPreferenceAsDefault(appData, gridOptions) {
    if (appData) {
      const preference = this.buildPreference(appData, gridOptions);
      preference.isDefault = true;
      return this.tablePreferenceService.markAsDefault(preference);
    }
  }

  updateLocalPreferences(data, opType) {
    if (opType == 'save') {
      this.tablePreferences.push(data);
      this.viewPreferencesSubject.next(this.tablePreferences);
    } else if (opType == 'update') {
      this.tablePreferences = this.tablePreferences.map(preference => preference.id == data.id ? data : preference);
      this.viewPreferencesSubject.next(this.tablePreferences);
    } else if (opType == 'delete') {
      this.tablePreferences = this.tablePreferences.filter(preference => preference.id != data.id);
      this.viewPreferencesSubject.next(this.tablePreferences);
    }
  }

  buildPreference(appData, gridOptions) {
    const preference: Preference = this.tablePreferenceService.getEmptyPreference(appData.id, appData.name, appData.appName, appData.gridName, appData.createdBy);
    Object.assign(preference, this.getState(gridOptions));
    return preference;
  }

  /***
   * Table Preference Context Menu options
   */
  gridPreferenceMenu(appName, gridName, userId, params, dialogRef, tablePreferenceList): any {

    return ['separator',
      {
        name: 'Open Table Preference',
        subMenu:
          tablePreferenceList.filter((template) => (template.gridName === params.context.gridName))
            .filter(data => {
              if (params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName)) {
                if (params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName).name === data.name) {
                  data.isSelected = true;
                } else {
                  data.isSelected = false;
                }
              }
              return true;
            })
        .map((currentState) => ({
            name: currentState.name ,
            cssClasses: currentState.isSelected ? ['selected-preference'] : [],
            action() {
              params.context.tablePreferenceDelegate.setViewPreference(params.context.gridOptions, currentState);
              const preference = params.context.tablePreferenceDelegate.setSelectedViewPreference(params.context.gridName, currentState.id, currentState.name, userId, currentState.appName, currentState.isDefault);

            }
          })).concat({
            name: 'Default',
            action() {
              const currentState = {
                name : 'Default',
                createdBy : userId,
                gridName : gridName,
                appName : appName,
                isDefault : false,
                id : '',
                columnGroups: [],
                sortModel: []
              };
              params.context.tablePreferenceDelegate.setViewPreference(params.context.gridOptions, currentState);
              const preference = params.context.tablePreferenceDelegate.setSelectedViewPreference(params.context.gridName, 'default-id', currentState.name, userId, appName, true);

            }
          })
      },
      {
        name: 'Save Table Preference',
        disabled: params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName) &&
          params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName).id != 'default-id' ? false : true,
        action() {
          const preference = params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName);
          if (preference) {
            preference.appName = appName;
            params.context.tablePreferenceDelegate.updateViewPreference(preference, params.context.gridOptions).subscribe(data => {
              params.context.tablePreferenceDelegate.updateLocalPreferences(data, 'update');
            });
          }
        }
      },
      {
        name: 'Delete Table Preference',
        disabled: params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName) &&
          params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName).id != 'default-id' ? false : true,
        action() {
          const preference = params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName);
          if (preference) {
            params.context.tablePreferenceDelegate.deleteViewPreference(preference).subscribe(data => {
                if (data) {
                  const currentState = {
                    name: 'Default'
                  };
                  const preference = params.context.tablePreferenceDelegate.setSelectedViewPreference(params.context.gridName, 'default-id', currentState.name, userId, appName, false);
                  params.context.tablePreferenceDelegate.setViewPreference(params.context.gridOptions, currentState);
                  params.context.tablePreferenceDelegate.updateLocalPreferences(data, 'delete');
                }
            });
          }
        }
      },
      {
        name: 'Save Table Preference As ',
        action() {
          const dialogRef2 = dialogRef.open(DialogComponent, {
            width: '500px',
            data: { name: '', isPreference: true, viewNames: tablePreferenceList.map(p => p.name) }
          });

          dialogRef2.afterClosed().subscribe(name => {
            if (name) {
              const preference: Preference = params.context.tablePreferenceDelegate.tablePreferenceService.getEmptyPreference(null, name, appName, gridName, userId);
              preference.isDefault = false;
              preference.name = name;
              params.context.tablePreferenceDelegate.saveViewPreference(userId, preference, params.context.gridOptions).subscribe(data => {
                if (data) {
                  const preference = params.context.tablePreferenceDelegate.setSelectedViewPreference(params.context.gridName, data.id, data.name, userId, appName, data.isDefault);
                  params.context.tablePreferenceDelegate.updateLocalPreferences(data, 'save');
                }
              });
            }
          });
        }
      },
      {
        name: 'Mark Default Table Preference',
        disabled: params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName) &&
          params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName).id != 'default-id' ? false : true,
        action() {
          const preference = params.context.tablePreferenceDelegate.getSelectedViewPreference(params.context.gridName);
          if (preference) {
            preference.appName = appName;
            params.context.tablePreferenceDelegate.updateViewPreferenceAsDefault(preference, params.context.gridOptions).subscribe(data => {
                if (data) {
                  params.context.tablePreferenceDelegate.updateLocalPreferences(data, 'update');
                }
            });
          }
        }
      }];
  }


}
