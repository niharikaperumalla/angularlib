import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Preference} from '../model';
import {libEnvironment} from '../lib-environment';


@Injectable({
  providedIn: 'root'
})
export class TablePreferenceService {

  /***To hold temporary state */
  private gridState: Map<string, object> = new Map();

  constructor(private httpClient: HttpClient) {}

  get(appName, gridName, userId): Observable<any | null> {
    return this.httpClient.get<any>(libEnvironment.baseUrl + 'tableviews', {
      params: {
        appName: appName,
        userId: userId,
      }
    });
  }

  save(userId, preference): Observable<any | null> {
    return this.httpClient.post<any>(libEnvironment.baseUrl + 'tableviews', preference, {
      params: {
        userId: userId
      }
    });
  }

  update(id, preference): Observable<any | null> {
    return this.httpClient.patch<any>(libEnvironment.baseUrl + 'tableviews/' + id, preference);
  }

  delete(id): Observable<any | null> {
    return this.httpClient.delete<any>(libEnvironment.baseUrl + 'tableviews/' + id);
  }

  markAsDefault(preference) {
    return this.httpClient.patch<any>(libEnvironment.baseUrl + 'tableviews' + '/' + preference.id, preference);
  }


  getGridState(): Map<string, object> {
    return this.gridState;
  }

  getEmptyPreference(id: string, name: string, appName: string, gridName: string, userId: string): Preference {
    return {
      id: id,
      gridName: gridName,
      appName: appName,
      name: name,
      createdBy: '' + userId,
      createdAt: '',
      isDefault: false,
      columns: null,
      columnGroups: null,
      sortModel: null,
      toolPanel: false
    } as Preference;
  }
}
