import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {DatePipe} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {GridActions} from '../common/models/grid-actions';
import {TablePreferenceDelegate} from '../table-preference/table-preference.delegate';
import {TablePreferenceService} from '../table-preference/table-preference.service';
import {Subscription} from 'rxjs';
import {ImportDialogComponent} from '../common/dialog/importexport/import-dialog.component';
import {ImportExportParam, featuresFlags} from '../model';
import {ImportExportService} from '../common/dialog/importexport/import-export.service';
import { AddrowComponent } from '../common/dialog/addrow/addrow.component';


@Component({
  selector: 'custom-grid-container',
  templateUrl: './grid-container.component.html',
  styleUrls: ['./grid-container.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GridContainerComponent implements OnInit {

  /**Input Properties */

  @Input() showSpinner: boolean;
  @Input() rowData: Array<any> = [];
  @Input() gridName: string;
  @Input() appName: string;
  @Input() userId: string;
  @Input() importExportParam: ImportExportParam;
  @Input() includeSearchToolbar: boolean;
  @Input() gridOptions: GridOptions;
  @Input() columnDefs: Array<any> = [];
  @Input() frameworkComponents: Array<any> = [];
  @Input() getNodeChildDetails?: any;
  @Input() pagination: boolean;
  @Input() defaultSearchCriteria: any;
  @Input() featureOptions: featuresFlags;
  @Input() searchFields: any;
  @Output() onRefreshClick: EventEmitter<GridActions> = new EventEmitter<GridActions>();
  @Output() filterValueChanged: EventEmitter<String> = new EventEmitter();
  @Output() onSearchCriteriaClick: EventEmitter<any> = new EventEmitter();
  @Output() onSearchCriteriaChange: EventEmitter<any> = new EventEmitter();
  @Output() onCellClicked: EventEmitter<any> = new EventEmitter();
  @Output() onCellDoubleClicked: EventEmitter<any> = new EventEmitter();
  @Output() onRowDoubleClicked: EventEmitter<any> = new EventEmitter();
  @Output() onRowSelected: EventEmitter<any> = new EventEmitter();
  @Output() onGridReady: EventEmitter<any> = new EventEmitter();

  /**end Input Properties  */

  /**Properties */
  filterString: string;
  rowCount: number;
  sideBar = false;
  gridActions = GridActions;
  datePipe: DatePipe = new DatePipe('en-US');
  searchCriteria: any;
  isDrawerOpened = false;
  tablePreferenceList = [];
  preferenceSubscription: Subscription;
  /**end Properties */

  /**Constructor */
  constructor(private tablePreferenceService: TablePreferenceService, private tablePreferenceDelegate: TablePreferenceDelegate, public dialog: MatDialog, public importExportService: ImportExportService) {
  }
  /**end Constructor */


  ngOnInit() {
    this.tablePreferenceDelegate.getPreferences(this.appName, this.gridName, this.userId);
    this.initTemplatePreferenceList();
    this.initGridOptions();
  }

  initTemplatePreferenceList() {
    const sortState = this.tablePreferenceDelegate.getGridState(this.gridName + '-GRID-SORT');
    const colState = this.tablePreferenceDelegate.getGridState(this.gridName + '-GRID-COLUMNS');
    const colGrpState = this.tablePreferenceDelegate.getGridState(this.gridName + '-GRID-COLUMNS-GROUPS');
    const preserveState = sortState || colState || colGrpState;
    const viewName = this.gridName;
    this.preferenceSubscription = this.tablePreferenceDelegate.getPreferencesViewSubject().subscribe(preferences => {
      const prefList = this.tablePreferenceList = preferences.filter(preference => {
        if (preference.gridName === viewName) {
          if (preference.isDefault && !preserveState) {
            this.tablePreferenceDelegate.setSelectedViewPreference(viewName, preference.id, preference.name, this.userId, preference.appName, preference.isDefault);
            this.tablePreferenceDelegate.selectedPreferenceSubject.next(preference);
          }
          return preference;
        }
      });
    });
  }


  initGridOptions() {
    // Initialize common grid properties.
    this.gridOptions.rowSelection = 'multiple';
    this.gridOptions.suppressLoadingOverlay = true;
    this.gridOptions.excelStyles = [{ id: 'textFormat', dataType: 'string' }];
    this.gridOptions.isExternalFilterPresent = this.isExternalFilterPresent;
    this.gridOptions.doesExternalFilterPass = this.doesExternalFilterPass;
    this.gridOptions.suppressLoadingOverlay = true;
    this.gridOptions.getContextMenuItems = (params) => this.getContextMenuItems(params, this.dialog, this.tablePreferenceList);

    //Register common grid event handlers.
    this.gridOptions.onCellClicked = (event) => { this.cellClicked(event); };
    this.gridOptions.onCellDoubleClicked = (event) => { this.cellDoubleClicked(event); };
    this.gridOptions.onRowDoubleClicked = (event) => { this.rowDoubleClicked(event); };
    this.gridOptions.onRowSelected = (event) => { this.rowSelected(event); };
    this.gridOptions.onGridReady = (event) => { this.gridReady(event); };

    if (this.getNodeChildDetails) {
      this.gridOptions.getNodeChildDetails = this.getNodeChildDetails;
    }

  }

  getRowCount(): number {
    if (this.gridOptions) {
      return this.gridOptions.api ? this.gridOptions.api.getModel().getRowCount() : 0;
    }
    return 0;
  }

  isGridDataAvailable() {
    if (this.gridOptions.api) {
      return this.gridOptions.api.getModel() ? true : false;
    }
    return false;
  }

  /**Common grid event handler methds */

  cellClicked(event) {
    this.onCellClicked.emit(event);
  }

  cellDoubleClicked(event) {
    this.onCellDoubleClicked.emit(event);
  }

  rowDoubleClicked(event) {
    this.onRowDoubleClicked.emit(event);
  }

  addRowDialog(): void {
    let dialogRef = this.dialog.open(AddrowComponent, {
      width: '600px',
      data: { formData:this.searchFields }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  rowSelected(event) {
    this.onRowSelected.emit(event);
  }

  gridReady(event) {
    //Set grid state
    setTimeout(() => {
      const sortState = this.tablePreferenceDelegate.getGridState(this.gridName + '-GRID-SORT');
      const colState = this.tablePreferenceDelegate.getGridState(this.gridName + '-GRID-COLUMNS');
      const colGrpState = this.tablePreferenceDelegate.getGridState(this.gridName + '-GRID-COLUMNS-GROUPS');
      if (sortState && event.api) {
        event.api.setSortModel(sortState);
      }

      if (colState) {
        event.columnApi.setColumnState(colState);
      }

      if (colGrpState) {
        event.columnApi.setColumnGroupState(colGrpState);
      }
      this.onGridReady.emit(event);
    }, 1000);
  }


  /**Common grid event handler methds */

  /**Custom context menu */
  getContextMenuItems(params, dialogRef, tablePreferenceList) {
    let result = [
      'copy',
      {
        name: 'Copy Cell',
        action: function () {
          const textarea = document.createElement('textarea');
          textarea.style.height = '0px';
          textarea.style.left = '-100px';
          textarea.style.opacity = '0';
          textarea.style.position = 'fixed';
          textarea.style.top = '-100px';
          textarea.style.width = '0px';
          document.body.appendChild(textarea);
          textarea.value = params.value ? params.value : '\r';
          textarea.select();
          // Ask the browser to copy the current selection to the clipboard.
          document.execCommand('copy');
          // Delete temporary element
          if (textarea && textarea.parentNode) {
            textarea.parentNode.removeChild(textarea);
          }
        }
      },
      'separator',
      {
        name: 'Auto Size Columns',
        action: () => { this.gridOptions.columnApi.autoSizeAllColumns(); }
      },
      {
        name: 'Size To Fit',
        action: () => { this.gridOptions.api.sizeColumnsToFit(); }
      },
      {
        name: 'Export',
        subMenu: [{
          name: 'Excel Export (.xlsx)',
          action: () => { this.gridOptions.api.exportDataAsExcel(); }
        },
        {
          name: 'CSV Export',
          action: () => { this.gridOptions.api.exportDataAsCsv(); }
        }
        ]

      }
    ];
    if (params && params.context.tablePreferenceDelegate) {
      /** Get Preferences Menu Options from Delegate */
      result = result.concat(params.context.tablePreferenceDelegate.gridPreferenceMenu(this.appName, this.gridName, this.userId, params, dialogRef, tablePreferenceList));
    }
    return result;
  }
  /**end Custom context menu */


  /** Quick Filter Related Functions */
  isExternalFilterPresent = (): boolean => {
    return !(this.filterString === undefined || this.filterString === '');
  }

  doesExternalFilterPass = (node): boolean => {
    for (const columnDef of this.gridOptions.columnDefs) {
      const colDef = columnDef as any;
      if (columnDef.hasOwnProperty('children')) {
        let length = columnDef['children'].length - 1;
        while (length >= 0) {
          const underKeys = columnDef['children'][length].field.split('.');
          let colValues = node.data;
          underKeys.forEach((key) => {
            colValues = colValues[key];
          });
          if (colValues && colValues.toString().search(new RegExp(this.filterString, 'i')) > -1) {
            return true;
          } else {
            length -= 1;
          }
        }
      } else {
        const keys = colDef.field.split('.');
        let colValue;
        if (node.data[keys[0]] instanceof Array) {
          colValue = JSON.stringify(node.data[keys[0]][0]);
        } else {
          colValue = node.data;
          keys.forEach((key) => {
            colValue = colValue[key];
          });
        }
        if (colValue && colValue.toString().search(new RegExp(this.filterString, 'i')) > -1) {
          return true;
        }
      }
    }
    return false;
  }


  externalFilterChanged(filterInput: string) {
    this.filterString = filterInput;
    this.gridOptions.api.onFilterChanged();
  }
  /**end Quick Filter Related Functions  */


  /**Search Tool bar */
  searchCriteriaClick(event) {
    this.searchCriteria = event.searchCriteria;
    this.isDrawerOpened = event.isAdd ? true : !this.isDrawerOpened;
    // this.onSearchCriteriaClick.emit(event);
  }

  refreshGrid(event) {
    this.searchCriteria = event;
    this.onSearchCriteriaChange.emit(event);
  }

  /**End search Tool bar */

  /**
   * Common Grid Actions
   * @param $event
   */
  onGridActionButtonClicked($event: GridActions) {
    switch ($event) {
      case GridActions.exportToExcel:
        this.exportToExcel();
        break;
      case GridActions.autoSizeAllColumns:
        this.autoSizeAllColumns();
        break;
      case GridActions.sizeColumnsToFit:
        this.sizeColumnsToFit();
        break;
      case GridActions.toggleToolPanel:
        this.toggleToolPanel();
        break;
      case GridActions.refreshGrid:
        this.onRefreshClick.emit($event);
    }
  }

  /**
   * Export to Excel Grid Action
   */
  exportToExcel() {
    const ref = this;
    const expParams = {
      skipHeader: false,
      skipFooters: true,
      processCellCallback: function (params) {
        let answers = '';
        if (params.value && Array.isArray(params.value)) {
          if (params.value.length === 1) {
            return params.value[0].value;
          } else {
            params.value.forEach((data) => {
              answers += (data.value + ' ,');
            });
            return answers;
          }
        } else {
          if (params.column.colDef.fieldType && params.column.colDef.fieldType === 'date') {
            return ref.formatDate(params.value);
          } else if (params.column.colDef.fieldType && params.column.colDef.fieldType === 'user') {
            return ref.getNameFromFirstAndLastName(params.value ? params.value.firstName : null,
              params.value ? params.value.lastName : null);
          }
          return params.value;
        }
      }
    };
    this.gridOptions.api.exportDataAsExcel(expParams);
  }

  /**
   * Auto Size Grid Action
   */
  autoSizeAllColumns() {
    if (this.gridOptions.columnApi) {
      const ref = this;
      setTimeout(function () {
        ref.gridOptions.columnApi.autoSizeAllColumns();
      }, 500);
    }
  }

  /**
   * ReSizetofit Grid Action
   */
  sizeColumnsToFit() {
    if (this.gridOptions.api) {
      const ref = this;
      setTimeout(function () {
        ref.gridOptions.api.sizeColumnsToFit();
      }, 500);
    }
  }


  /**
   * Tool Panel Action
   */
  toggleToolPanel() {
    this.sideBar = !this.sideBar;
  }

  /**Helper Methods */

  formatDate(date) {
    if (!date) {
      return '';
    }
    return this.datePipe.transform(date, 'yyyy-MM-dd HH:mm');
  }

  getNameFromFirstAndLastName(firstName, lastName) {
    firstName = firstName ? firstName : '';
    lastName = lastName ? lastName : '';
    const name = firstName + ' ' + lastName;
    return name.trim();
  }

  /*Import and Export functions*/
  import() {
    const data = this.importExportParam;
    const dialogRef = this.dialog.open(ImportDialogComponent, {
      width: '600px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      this.onRefreshClick.emit(GridActions.refreshGrid);
    });
  }

  export() {
    this.importExportService.export(this.importExportParam, false);
  }
  /**end Helper Methods*/

  ngOnDestroy() {
    this.preferenceSubscription.unsubscribe();
  }

}
